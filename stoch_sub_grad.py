"""
This module contains an implementation of the stochastic sub-gradient descent
flavor of support vector machines (SVM).
"""

from copy import deepcopy
import matplotlib.pyplot as plt
import numpy as np
import random


class SVMClassifier(object):
    """
    A Support Vector Machine classifier using the stochastic sub-gradient
    descent implementation.
    """

    def __init__(self):
        """ Constructor """
        self.weight_vector = None
        self.num_updates = 0
        self.loss_vec = np.array([])

    def train(self,
              features,
              labels,
              gamma0=0.01,
              gamma_update=lambda g0, t: float(float(g0) / (1.0 + float(t))),
              epochs=10,
              C=1.0):
        """
        Trains an SVM learner with a matrix of training data and a vector
        of labels that classify the training data.

        features:       The matrix of training data.
        labels:         The vector of labels that classify the training data.
        gamma0:         The initial value for the learning rate.
        gamma_update:   The function to update gamma on each iteration.
        epochs:         The number of epochs to run through.
        C:              A hyperparameter that controls the tradeoff between the
                        regularization term and the risk minimization term in
                        the SVM loss function.
        """
        N = features.shape[0]
        sample_inds = range(N)
        random.seed()
        self.weight_vector = np.zeros(features.shape[1])
        self.num_updates = 0
        self.loss_vec = np.array([])
        gamma = float(gamma0)
        broke_margin_count = 0
        for epoch in xrange(epochs):
            # Shuffle the data
            random.shuffle(sample_inds)
            for i in xrange(len(sample_inds)):
                # Get the feature vector and the actual label at the specified
                # index.
                x_i = features[sample_inds[i]]  # vector
                y_i = int(deepcopy(labels[sample_inds[i]]))
                if 0 == y_i:
                    y_i = -1

                # Calculate the loss at this point.
                self.loss_vec = np.append(self.loss_vec,
                                          self.calculate_loss(C, N, x_i, y_i))

                # Check to see if the sample breaks the margin.
                func_marg_dist = float(y_i) * np.dot(self.weight_vector, x_i)
                if (func_marg_dist <= 1.0):
                    # If the sample breaks the margin, we use it as a support
                    # vector and update our weight vector accordingly.
                    self.weight_vector = (
                        (1.0 - gamma) * self.weight_vector) + (
                            gamma * C * float(N) * float(y_i) * x_i)
                    broke_margin_count += 1
                else:
                    # If the sample doesn't break the margin, we don't use it as
                    # a support vector in updating the weight vector.
                    self.weight_vector = (1.0 - gamma) * self.weight_vector

                # Update the learning rate according to the given schedule.
                gamma = gamma_update(gamma0, self.num_updates + 1)
                self.num_updates += 1

    def calculate_loss(self, C, N, sample, label):
        """
        Calculates the value of the SVM loss function with the current weight
        vector over the training data.

        Arguments:
            C:      A hyperparameter that controls the tradeoff between the
                    regularization term and the risk minimization term.
            N:      The number of training samples. 
            sample: The sample feature vector.
            label:  The label corresponding to the sample vector.
        
        Returns:
            The value of the loss function using the current weight vector.
        """
        label_in = deepcopy(label)
        if 0 == label_in:
            label_in = -1

        max_term = 1.0 - (float(label_in) * np.dot(self.weight_vector, sample))
        if (max_term < 0.0):
            max_term = 0.0

        loss = 0.5 * np.dot(
            self.weight_vector,
            self.weight_vector) + float(C) * float(N) * max_term

        return loss

    def plot_loss(self, ax, C_num, C_denom, gamma0, d=0.0, use_d=False):
        """
        Plots the value of the loss function as a function of time (where "time"
        refers to how many times the weight vector was updated).

        Arguments:
            ax:             The axes object on which to make the plot.
            plot_title:     The title to apply to the plot.
            C_num:          The numerator of the C hyperparameter.
            C_denom:        The denominator of the C hyperparameter.
            gamma0:         The value of the gamma0 parameter.
            d:              The parameter d in the learning rate schedule.
            use_d:          A flag indicating whether or not we will use d.
        """
        t = np.array(range(self.num_updates), dtype=float)
        t /= 1000.0

        t_reduced = np.array([])
        loss_reduced = np.array([])

        for i in xrange(len(t)):
            if i % 100 == 0:
                t_reduced = np.append(t_reduced, t[i])
                loss_reduced = np.append(loss_reduced, self.loss_vec[i])

        ax.plot(t_reduced, loss_reduced)

        if use_d:
            ax.set(xlabel='Time (updates) x 1000',
                   ylabel='Loss',
                   title=r'C = {}/{}, $\gamma_0 = ${}, $d = ${}'.format(
                       int(C_num), int(C_denom), gamma0, d))
        else:
            ax.set(xlabel='Time (updates) x 1000',
                   ylabel='Loss',
                   title=r'C = {}/{}, $\gamma_0$ = {}'.format(
                       int(C_num), int(C_denom), gamma0))
        ax.grid()

    def predict(self, feature_vec):
        """
        Predicts the label of a given feature vector based on the weight vector
        learned by the classifier.

        Arguments:
            feature_vec:    The vector of features for which we want to predict
                            a label.

        Returns:
            The label predicted by the classifier.
        """
        mult = np.dot(self.weight_vector, feature_vec)
        sign = np.sign(mult)
        if -1 == sign:
            sign = 0

        return int(sign)

    def accuracy_score(self, features, labels):
        """
        Reports the accuracy as a floating point value between zero (0%) and
        one (100%).

        Arguments:
            features:   The matrix of features to test.
            labels:     The actual labels that classify the input features.

        Returns:
            The accuracy score of the classifier's learned weight vector.
        """
        denominator = features.shape[0]
        numerator = 0
        for i in xrange(denominator):
            pred = self.predict(features[i])

            # Mark as correct if the prediction equals the actual label.
            if labels[i] == pred:
                numerator += 1

        # Compute the ratio.
        return float(numerator) / float(denominator)
