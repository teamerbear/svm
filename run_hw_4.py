"""
A script that runs all the calculations for Homework 4.
"""

from extractor import extractor
import matplotlib.pyplot as plt
import numpy as np
from stoch_sub_grad import SVMClassifier

if __name__ == "__main__":
    # Extract the data from the data files.
    features_train, labels_train = extractor.extract(
        "data/svm_train.csv", append_bias=True)
    features_test, labels_test = extractor.extract(
        "data/svm_test.csv", append_bias=True)

    # Create an array of the different C parameters we are to try.
    C_nums = np.array([10, 100, 300, 500, 700], dtype=float)
    C_denom = 873.0
    C_arr = C_nums / C_denom

    # Create the two different learning rate update schedules.
    default_sched = lambda g0, t: float(float(g0) / (1.0 + float(t)))

    # Now train classifiers based on different C and rate schedules.
    fig = plt.figure()
    fig.set_figheight(20)
    fig.set_figwidth(30)
    for i in xrange(len(C_arr)):
        g0 = 1.25
        clf = SVMClassifier()
        clf.train(
            features_train,
            labels_train,
            gamma0=g0,
            gamma_update=default_sched,
            C=C_arr[i],
            epochs=50)
        acc = clf.accuracy_score(features_test, labels_test)
        print "Error for C = {}/{}, gamma0 = {} and Schedule 1 is {}".format(
            int(C_nums[i]), int(C_denom), g0, 1.0 - acc)
        print "Final Weight Vector: {}\n".format(clf.weight_vector)

        ax = fig.add_subplot((231 + i))
        plt.ylim(ymax=100)
        clf.plot_loss(ax, C_nums[i], C_denom, g0)

    fig.subplots_adjust(hspace=0.2, wspace=0.3)
    fig.savefig("schedule_1.png")

    # Clear the figure so we can make another plot.
    plt.clf()

    my_d = 0.75
    other_sched = lambda g0, t: float(float(g0) / (1.0 + (float(g0) / my_d) * float(t)))

    print "\n"
    fig = plt.figure()
    fig.set_figheight(20)
    fig.set_figwidth(30)
    for i in xrange(len(C_arr)):
        g0 = 1.0
        clf = SVMClassifier()
        clf.train(
            features_train,
            labels_train,
            gamma0=g0,
            gamma_update=other_sched,
            C=C_arr[i],
            epochs=50)
        acc = clf.accuracy_score(features_test, labels_test)
        print "Error for C = {}/{}, gamma0 = {}, d = {}, and Schedule 2 is {}".format(
            int(C_nums[i]), int(C_denom), g0, my_d, 1.0 - acc)
        print "Final Weight Vector: {}\n".format(clf.weight_vector)

        ax = fig.add_subplot((231 + i))
        plt.ylim(ymax=100)
        clf.plot_loss(ax, C_nums[i], C_denom, g0, d=my_d, use_d=True)

    fig.subplots_adjust(hspace=0.2, wspace=0.3)
    fig.savefig("schedule_2.png")