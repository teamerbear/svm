To run the code for Homework 4, simply run the 'run.sh' script:
    ./run.sh

The program will output the test error for each C setting to the console
along with the parameters that made the algorithm converge. Two files will be
created containing plots that demonstrate the convergence of the two learning
rate schedules, schedule_1.png and schedule_2.png.  The 'schedule_1' file
corresponds to the schedule defined in part (b) of the programming portion of
the assignment and the 'schedule_2' file corresponds to the schedule defined in
part (a).
